package com.example.springhomeiii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringhomeIiiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringhomeIiiApplication.class, args);
    }

}
