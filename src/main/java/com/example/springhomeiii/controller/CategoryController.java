package com.example.springhomeiii.controller;


import com.example.springhomeiii.model.entity.Category;
import com.example.springhomeiii.model.request.CategoryRequest;
import com.example.springhomeiii.model.response.ApiResponse;
import com.example.springhomeiii.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class CategoryController {
    private final CategoryService categoryService;


    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categorys")
    public List<Category> getAllCategory() {

        return categoryService.getAllCategory();
    }

    @GetMapping("/categorys/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable Integer id) {
        Category category = categoryService.getCategoryById(id);
        ApiResponse<Category> response = new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                category
        );
        return ResponseEntity.ok(response);
    }

    @PostMapping("/categorys")
    public ResponseEntity<?> insertCategory(@RequestBody CategoryRequest categoryRequest) {
        return ResponseEntity.ok(new ApiResponse<Category>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                categoryService.insertCategory(categoryRequest)
        ));
    }

    @PutMapping("/categorys/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable Integer id, @RequestBody CategoryRequest categoryRequest) {
        categoryService.updateCategoryById(id, categoryRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                null
        ));
    }

    @DeleteMapping("/categorys/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") Integer id) {
        categoryService.deleteCategoryById(id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                null
        ));
    }
}












