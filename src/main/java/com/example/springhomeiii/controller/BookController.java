package com.example.springhomeiii.controller;


import com.example.springhomeiii.model.entity.Book;
import com.example.springhomeiii.model.response.ApiResponse;
import com.example.springhomeiii.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class BookController {
    private final BookService bookService;


    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/books")
    public ResponseEntity<?> getAllBook() {
        return ResponseEntity.ok(
                new ApiResponse<List<Book>>(
                        LocalDateTime.now(),
                        HttpStatus.OK,
                        "Successful",
                        bookService.getAllBook()

                )
        );
    }
}
