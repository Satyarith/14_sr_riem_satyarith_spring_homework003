package com.example.springhomeiii.controller;


import com.example.springhomeiii.model.request.AuthorRequest;
import com.example.springhomeiii.model.response.ApiResponse;
import com.example.springhomeiii.model.entity.Author;
import com.example.springhomeiii.service.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
//@RequestMapping("api/v1/author")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {

        this.authorService = authorService;
    }

    @GetMapping("/author")
    public List<Author> getAllAuthor(){

        return authorService.getAllAuthor();
    }

    @GetMapping("/author/{id}")
    public ResponseEntity<?> getAuthorById(@PathVariable Integer id){
        Author author = authorService.getAuthorById(id);
        ApiResponse<Author> response = new ApiResponse<>(
                LocalDateTime.now(),
        HttpStatus.OK,
                "Successful",
                author
        );
        return ResponseEntity.ok(response);
    }

    @PostMapping("/author")
    public ResponseEntity<?> insertAuthor(@RequestBody AuthorRequest authorRequest){
        return ResponseEntity.ok(new ApiResponse<AuthorRequest>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                authorService.insertAuthorRequest(authorRequest)

        ));
    }
    @PutMapping("/authors/{id}")
    public ResponseEntity<?> updateAuthor(@PathVariable Integer id, @RequestBody AuthorRequest authorRequest){
        authorService.updateAuthorById(id, authorRequest);
        return ResponseEntity.ok(new ApiResponse<>(
            LocalDateTime.now(),
            HttpStatus.OK,
            "Successful",
            null
    ));

    }
    @DeleteMapping("/authors/{id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable("id") Integer id){
        authorService.deleteAuthorById(id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                null
        ));
    }
}




















