package com.example.springhomeiii.service;


import com.example.springhomeiii.model.entity.Book;

import java.util.List;

public interface BookService {


    List<Book> getAllBook();

}
