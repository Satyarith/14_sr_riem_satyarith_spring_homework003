package com.example.springhomeiii.service;


import com.example.springhomeiii.model.entity.Author;
import com.example.springhomeiii.model.request.AuthorRequest;
import com.example.springhomeiii.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImp implements AuthorService{

    private final AuthorRepository authorRepository;
@Autowired
    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;

    }

    @Override
    public List<Author> getAllAuthor() {

    return authorRepository.getAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer id) {

    return authorRepository.getAuthorById(id);
    }

    @Override
    public Author insertAuthor(AuthorRequest authorRequest) {

    return authorRepository.insertAuthor(authorRequest);
    }

    @Override
    public void updateAuthorById(Integer id, AuthorRequest authorRequest) {
     authorRepository.updateAuthor(id, authorRequest);
    }

    @Override
    public void deleteAuthorById(Integer id) {
        authorRepository.deleteAuthorById(id);
    }

    @Override
    public AuthorRequest insertAuthorRequest(AuthorRequest authorRequest) {
        return null;
    }

}
















