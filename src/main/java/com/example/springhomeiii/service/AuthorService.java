package com.example.springhomeiii.service;

import com.example.springhomeiii.model.entity.Author;
import com.example.springhomeiii.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();

    Author getAuthorById(Integer id);

    Author insertAuthor(AuthorRequest authorRequest);

    void updateAuthorById(Integer id, AuthorRequest authorRequest);

    void deleteAuthorById(Integer id);

    AuthorRequest insertAuthorRequest(AuthorRequest authorRequest);

}
