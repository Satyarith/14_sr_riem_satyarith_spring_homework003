package com.example.springhomeiii.service;


import com.example.springhomeiii.model.entity.Category;
import com.example.springhomeiii.model.request.CategoryRequest;
import com.example.springhomeiii.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

private final CategoryRepository categoryRepository;
@Autowired
    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public Category getCategoryById(Integer id) {
        return categoryRepository.getCategoryById(id) ;
    }

    @Override
    public Category insertCategory(CategoryRequest categoryRequest) {
        return categoryRepository.insertCategory(categoryRequest);
    }

    @Override
    public void updateCategoryById(Integer id, CategoryRequest categoryRequest) {
    categoryRepository.updateCategory(id, categoryRequest);
    }

    @Override
    public void deleteCategoryById(Integer id) {
    categoryRepository.deleteCategoryById(id);
    }

}
