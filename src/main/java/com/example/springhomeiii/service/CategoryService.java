package com.example.springhomeiii.service;


import com.example.springhomeiii.model.entity.Category;
import com.example.springhomeiii.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {

 List<Category> getAllCategory();
 Category getCategoryById(Integer id);
 Category insertCategory(CategoryRequest categoryRequest);
 void updateCategoryById(Integer id, CategoryRequest categoryRequest);
 void deleteCategoryById(Integer id);

    

}
