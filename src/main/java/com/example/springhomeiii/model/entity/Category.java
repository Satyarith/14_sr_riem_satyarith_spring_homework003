package com.example.springhomeiii.model.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Category {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer category_id;
    private String category_name;
}
