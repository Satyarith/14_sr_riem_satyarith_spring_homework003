package com.example.springhomeiii.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
private Integer book_id;
private String book_title;
private LocalDateTime book_date;

private Author author;
private List<Category> category;

}
