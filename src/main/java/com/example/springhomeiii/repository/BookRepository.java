package com.example.springhomeiii.repository;


import com.example.springhomeiii.model.entity.Book;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BookRepository {

@Select("""
        SELECT * FROM book;
        """)
        @Result(property = "book_id", column = "book_id")
        @Result(property = "author", column = "author_id",
        one = @One(select = "com.example.springhomeiii.repository.AuthorRepository.getAuthorById")
        )
        @Result(property = "category", column = "book_id",
        many = @Many(select = "com.example.springhomeiii.repository.CategoryRepository.getAllBookById")
        )
    List<Book> getAllBook();

}
