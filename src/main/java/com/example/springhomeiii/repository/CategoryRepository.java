package com.example.springhomeiii.repository;


import com.example.springhomeiii.model.entity.Category;
import com.example.springhomeiii.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CategoryRepository  {

    @Select("""
            SELECT * FROM category
            """)
    List<Category> getAllCategory();

    @Select("""
           SELECT * FROM category 
           WHERE category_id =#{id};
            """)
    Category getCategoryById(Integer id);

    @Select("""
            INSERT INTO category(category_name)
            VALUES (#{category_name});
            """)
    Category insertCategory(CategoryRequest categoryRequest);

    @Update("""
            UPDATE category
            SET category_name = #{category.category_name}
            WHERE category_id= #{id};
            """)
    void updateCategory(Integer id, @Param("category") CategoryRequest categoryRequest);

    @Delete("""
                DELETE FROM category
                WHERE category_id = #{id};
                """)
    void deleteCategoryById(Integer id);

    @Select("""
            SELECT a.category_id, a.category_name
            FROM category a INNER JOIN book_details bd on a.category_id = bd.category_id
            WHERE book_id = #{book_id};
            """)
    @Result(property = "category_id", column = "category_id")
    List<Category> getAllBookById(Integer book_id);

}





















