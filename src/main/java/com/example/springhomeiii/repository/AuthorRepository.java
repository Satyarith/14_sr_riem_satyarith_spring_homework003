package com.example.springhomeiii.repository;

import com.example.springhomeiii.model.entity.Author;
import com.example.springhomeiii.model.request.AuthorRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AuthorRepository {
    @Select("""
            SELECT * FROM author""")

    List<Author> getAllAuthor();

    @Select("""
           SELECT * FROM author WHERE author_id =#{id};
            """)
            Author getAuthorById(Integer id);

    @Select("""
            INSERT INTO author(author_name, author_gender)
            VALUES (#{author_name}, #{author_gender});
            """)
            Author insertAuthor(AuthorRequest authorRequest);


    @Update("""
            UPDATE author
            SET author_name = #{author.author_name}, author_gender =#{author.author_gender}
            WHERE author_id= #{id};
            """)
            void updateAuthor(Integer id, @Param("author") AuthorRequest authorRequest);


    @Delete("""
                DELETE FROM author
                WHERE author_id = #{id};
                """)
     void deleteAuthorById(Integer id);

}



















