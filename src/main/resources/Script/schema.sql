


CREATE TABLE author(
    author_id SERIAL PRIMARY KEY ,
    author_name VARCHAR(50) NOT NULL ,
    author_gender VARCHAR(50) NOT NULL
);

CREATE TABLE category(
                         category_id SERIAL PRIMARY KEY ,
                         category_name VARCHAR(255) NOT NULL
);

CREATE TABLE book(
                     book_id SERIAL PRIMARY KEY ,
                     book_title VARCHAR(255),
                     book_date TIMESTAMP

);
    ALTER TABLE book
    ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES author (author_id);
CREATE TABLE book_details(
    book_id INT NOT NULL ,
    category_id INT NOT NULL,
    PRIMARY KEY (book_id, category_id),
    FOREIGN KEY (book_id) REFERENCES book (book_id) ON DELETE CASCADE ,
    FOREIGN KEY (category_id) REFERENCES category (category_id) ON DELETE  CASCADE
);







